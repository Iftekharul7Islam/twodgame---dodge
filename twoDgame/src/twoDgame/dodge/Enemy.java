package twoDgame.dodge;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

public class Enemy extends Entity{
	
	private Rectangle hitbox;
	private int ix,iy;
	private boolean dead=false;
	private Dodge instance;
	public Enemy(Dodge instance,int x,int y) {
		super(x,y);
		this.instance=instance;
		hitbox=new Rectangle(x,y,32,32);
		ix=0;
		iy=1;
		
	}
	private void move(){
		if(instance.getStage().iscollided(hitbox)){
			iy=0;
			dead=true;
			
		}
		hitbox.x+=ix;
		hitbox.y+=iy;
	}
	public boolean isDead(){ return dead; }
	public Rectangle getHitbox(){ return hitbox; }
	public void draw(Graphics g){
		move();
		g.setColor(Color.MAGENTA);
		g.fillRect(hitbox.x, hitbox.y, hitbox.width, hitbox.height);
		
	}

}
