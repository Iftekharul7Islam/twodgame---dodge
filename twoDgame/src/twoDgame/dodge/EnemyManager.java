package twoDgame.dodge;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class EnemyManager {
	private int amount;
	private List<Enemy>enemies=new ArrayList<Enemy>();
	private Dodge instance;
	
	public EnemyManager(Dodge instance,int a){
		this.amount=a;
		this.instance=instance;
		spawn();
	}
	public void spawn(){
		Random random=new Random();
		int stackSize=enemies.size();
		if(stackSize<amount){
			for(int i=0;i< amount-stackSize;i++){
				enemies.add(new Enemy(instance,random.nextInt(778),random.nextInt(100)));
			}
		}else if(stackSize > amount){
			for(int i=0;i< stackSize-amount;i++){
				enemies.remove(i);
			}
		}
	}
	
	public void draw(Graphics g){
		update();
		for(Enemy e:enemies)e.draw(g);
	}
	private void update(){
		boolean re=false;
		for(int i=0;i<enemies.size();i++){
			if(enemies.get(i).isDead()){
				enemies.remove(i);
				re=true;
			}
		}
		if(re) spawn();
	}
	public boolean isColliding1(Rectangle hitbox){
		boolean c=false;
		for(int i=0;i<enemies.size();i++){
			if(hitbox.intersects(enemies.get(i).getHitbox())) c=true;
			}
		return c;
		}
	public boolean isColliding11(Rectangle hitbox) {
		// TODO Auto-generated method stub
		return false;
	}
	public boolean isColliding(Rectangle hitbox) {
		// TODO Auto-generated method stub
		return false;
	}
}

