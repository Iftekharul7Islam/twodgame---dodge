package twoDgame.dodge;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.*;


public class Dodge extends JPanel implements KeyListener{        
	private Player player;
	private Stage stage;
	private boolean isGameOver=false;
	private EnemyManager manager;
	
	public Dodge(){
		setSize(new Dimension(800,600));
		setPreferredSize(new Dimension(800,600));
		//setBackground(Color.BLACK);
		setFocusable(true);
		addKeyListener(this);
		
		stage=new Stage();
		player=new Player(this,200, 200);
		manager=new EnemyManager(this, 10);
	}
	@Override
	public void update(Graphics g){
		paint(g);
	}
	
	
	public void paint(Graphics g){
		g.setColor(Color.CYAN);
		g.fillRect(0, 0, getWidth(), getHeight());
//		g.setColor(Color.ORANGE);
//		g.fillOval(x, y, 20, 20);
		
		stage.draw(g);
		if(!isGameOver){
		   player.draw(g);
		   manager.draw(g);
		}else{
		   g.setColor(Color.RED);
		   g.setFont(new Font("Century Gothic", Font.BOLD, 34));
		   g.drawString("Game Over!", 300, 300);
		}
		
		g.dispose();
		repaint();
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		int c=e.getKeyCode();
		if(c==KeyEvent.VK_W){
			//y-=5;
			//player.setYD(-1);
		}
		if(c==KeyEvent.VK_LEFT){
			//y+=5;
			player.setXD(-1);
		}
		if(c==KeyEvent.VK_S){
			//x-=5;
			//player.setYD(1);
		}
		if(c==KeyEvent.VK_RIGHT){
			//x+=5;
			player.setXD(1);
		}
	}
	public void setGameOver(boolean flag){
		isGameOver=flag;
	
	}
		

	@Override
	public void keyReleased(KeyEvent e) {
		player.setXD(0);
		player.setYD(0);
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	public Stage getStage(){
		return stage;
	}
	public EnemyManager getEnemyManager(){
		return manager;
	}
	
	


	public static void main(String[] args) {
		Dodge game=new Dodge();
		 JFrame frame=new JFrame();
		 frame.setTitle("Dodge the Rectangles");
		 frame.add(game);
		 frame.pack();
		 frame.setResizable(false);
		 frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		 frame.setLocationRelativeTo(null);
		 frame.setVisible(true);

	}

}
